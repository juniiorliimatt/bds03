package com.devsuperior.bds03.controllers.exceptions;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class ValidationError extends StandardError {

  private final List<FieldMessage> errors = new ArrayList<>();

  public ValidationError(Instant timestamp, int status, String errorMessage, String exceptionMessage, String path) {
	super(timestamp, status, errorMessage, exceptionMessage, path);
  }

  public void addError(String fieldName, String message) {
	errors.add(new FieldMessage(fieldName, message));
  }

  public List<FieldMessage> getErrors() {
	return errors;
  }

}
