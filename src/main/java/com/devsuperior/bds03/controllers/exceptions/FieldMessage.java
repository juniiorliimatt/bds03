package com.devsuperior.bds03.controllers.exceptions;

import java.io.Serializable;

public class FieldMessage implements Serializable {

  private final String fieldName;
  private final String message;

  public FieldMessage(String fieldName, String message) {
	this.fieldName = fieldName;
	this.message = message;
  }

  public String getFieldName() {
	return fieldName;
  }

  public String getMessage() {
	return message;
  }

}
