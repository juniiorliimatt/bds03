package com.devsuperior.bds03.services.exceptions;

public class ResourceNotFoundException extends RuntimeException {

  public ResourceNotFoundException(String message) {
	super(message);
  }

}
